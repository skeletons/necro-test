type ident

type bool = | T | F

val eq : (ident, ident) → bool

type clos = (ap : clos → clos)

type term =
| Lam (ident, term)
| Var ident
| App (term, term)

type env = ident → clos

val init : env

val extEnv ((e:env), (y:ident), (v:clos)) : env =
	λ x:ident → let b = eq (x, y) in
	match b with
	| T → v
	| F → e x
	end

val eval ((s:env), (l:term)): clos =
	match l with
	| Lam (x, t) →
		(ap = λ (v:clos) → let s' = extEnv (s, x, v) in eval (s', t))
	| Var x →
		s x
	| App (t1, t2) →
		let f = eval (s, t1) in
		let w = eval (s, t2) in
		f.ap w
	end

val main (t:term) : clos = eval (init, t)
