(* option monad *)
type option<a> =
| None
| Some a

val opt<a, b> ((o:option<a>), (f:(a → option<b>))): option<b> =
	match o with
	| None → None<b>
	| Some x → f x
	end

type ident
type lit

type value

type env
type int

type list<a> =
| Nil
| Cons (a, list<a>)

type mlterm =
| Lam (ident, mlterm)
| Var ident
| App (mlterm, mlterm)
| Const lit
| Plus (mlterm, mlterm)
| IfZero (mlterm, mlterm, mlterm)
| Constr (ident, list<mlterm>)
| Match (mlterm, list<(pattern, mlterm)>)

type pattern =
| PWildcard
| PVar ident
| POr (pattern, pattern)
| PConstr (ident, list<pattern>)


val mkClos : (ident, mlterm, env) → value
val mkInt : int → value
val getClos : value → (ident, mlterm, env)
val getInt : value → int

val mkConstr : (ident, list<value>) → value
val isConstrWithName : (ident, value) → list<value>
val isNotConstrWithName : (ident, value) → ()

val extEnv : (env, ident, value) → env
val getEnv : (ident, env) → value

val intOfLit : lit → int
val plus : (int, int) → int
val isZero : int → ()
val isNotZero : int → ()

val match_failure : () → value

val eval ((env,t):(env, mlterm)): value =
	match t with
	| Lam (x, body) →
		mkClos (x, body, env)
	| Var v →
		getEnv (v, env)
	| App (t1, t2) →
		let f = eval (env, t1) in
		let (x, body, env1) = getClos (f) in
		let v = eval (env, t2) in
		let env2 = extEnv (env1, x, v) in
		eval (env2, body)
	| Const n →
		let x = intOfLit (n) in
		mkInt (x)
	| Plus (t1, t2) →
		let v1 = eval (env, t1) in
		let x1 = getInt (v1) in
		let v2 = eval (env, t2) in
		let x2 = getInt (v2) in
		let x = plus (x1, x2) in
		mkInt (x)
	| IfZero (cond, ifz, ifnz) →
		let v = eval (env, cond) in
		let x = getInt (v) in
		branch
			isZero (x) ;
			eval (env, ifz)
		or
			isNotZero (x) ;
			eval (env, ifnz)
		end
	| Constr (name, tl) →
		let vl = eval_list (env, tl) in
		mkConstr (name, vl)
	| Match (tm, m) →
		let v = eval (env, tm) in
		mlmatching (env, v, m)
end

val eval_list ((env, t):(env, list<mlterm>)): list<value> =
	match t with
	| Nil →
		Nil<value>
	| Cons (t, tl) →
		let v = eval (env, t) in
		let vl = eval_list (env, tl) in
		Cons<value> (v, vl)
	end

val mlmatching ((env, v, l):(env, value, list<(pattern, mlterm)>)): value =
match l with
| Nil →
	match_failure ()
| Cons (x, m) →
	let (p, t) = x in
	let env1opt = eval_pattern (env, v, p) in
	match env1opt with
	| Some env1 →
		eval (env1, t)
	| None →
		let None = env1opt in
		mlmatching (env, v, m)
	end
end

val eval_pattern ((env, v, p):(env, value, pattern)): option<env> =
match p with
| PWildcard →
	Some<env> env
| PVar x →
	let env1 = extEnv (env, x, v) in
	Some<env> (env1)
| POr (p1, p2) →
	let env1opt = eval_pattern (env, v, p1) in
	match env1opt with
	| Some env1 →
		Some<env> env1
	| None →
		eval_pattern (env, v, p2)
	end
| PConstr (name, pl) →
	branch
		let args = isConstrWithName (name, v) in
		pattern_list (env, args, pl)
	or
		isNotConstrWithName (name, v) ;
		None<env>
	end
end

val pattern_list ((env, lv, lp):(env, list<value>, list<pattern>)): option<env> =
	match (lv, lp) with
	| (Nil, Nil) → Some<env> (env)
	| (Cons (v, t), Cons (p, pl)) →
		let env1 =%opt eval_pattern (env, v, p) in
		pattern_list (env1, t, pl)
	| _ → None<env>
	end

