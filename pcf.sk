type ident
type lit

type value
type env
type int

type lterm =
| Lam (ident, lterm)
| Var ident
| App (lterm, lterm)
| Const lit
| Plus (lterm, lterm)
| IfZero (lterm, lterm, lterm)

val mkClos : (ident, lterm, env) → value
val mkInt : int → value
val getClos : value → (ident, lterm, env)
val getInt : value → int

val extEnv : (env, ident, value) → env
val getEnv : (ident, env) → value

val intOfLit : lit → int
val plus : (int, int) → int
val isZero : int → ()
val isNotZero : int → ()


val eval ((s,t):(env, lterm)): value =
	match t with
	| Lam (x, body) →
		mkClos (x, body, s)
	| Var v →
		getEnv (v, s)
	| App (t1, t2) →
		let f = eval (s, t1) in
		let (x, body, env) = getClos (f) in
		let v = eval (s, t2) in
		let nenv = extEnv (env, x, v) in
		eval (nenv, body)
	| Const n →
		let x = intOfLit (n) in
		mkInt (x)
	| Plus (t1, t2) →
		let v1 = eval (s, t1) in
		let x1 = getInt (v1) in
		let v2 = eval (s, t2) in
		let x2 = getInt (v2) in
		let x = plus (x1, x2) in
		mkInt (x)
	| IfZero (cond, ifz, ifnz) →
		let v = eval (s, cond) in
		let x = getInt (v) in
		branch
			isZero (x) ;
			eval (s, ifz)
		or
			isNotZero (x) ;
			eval (s, ifnz)
		end
	end
